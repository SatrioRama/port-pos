<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::get('dashboard', 'AdminController@dashboard')->name('backpack.dashboard');
    Route::crud('user', 'UserCrudController');
    Route::crud('ticket', 'TicketCrudController');
    Route::crud('vehicle', 'VehicleCrudController');
    Route::post('Api/Vehicle', 'VehicleCrudController@getEdit');
    Route::post('Api/Shift', 'ShiftCrudController@getEdit');
    Route::crud('storebranch', 'StoreBranchCrudController');
    Route::crud('storebranchtype', 'StoreBranchTypeCrudController');
    Route::crud('producttype', 'ProductTypeCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('shift', 'ShiftCrudController');
    Route::get('shift/struk/{id}/print', 'ShiftCrudController@strukPdf');
    Route::crud('transaction', 'TransactionCrudController');
    Route::get('transaction/struk/{id}/print', 'TransactionCrudController@strukPdf');
    Route::get('transaction/ticket/{id}/print', 'TransactionCrudController@ticketPdf');
    Route::get('transaction/{id}/preview', 'TransactionCrudController@preview');
    Route::crud('transaction-detail', 'TransactionDetailCrudController');
    Route::crud('vehicle-time', 'VehicleTimeCrudController');
    Route::crud('role', 'RoleCrudController');
    Route::get('charts/vehicle-used', 'Charts\VehicleUsedChartController@response')->name('charts.vehicle-used.index');
    Route::crud('holiday', 'HolidayCrudController');
    Route::crud('report-transaction', 'ReportTransactionCrudController');
    Route::get('charts/kasir-chart', 'Charts\KasirChartChartController@response')->name('charts.kasir-chart.index');
    Route::get('charts/store-manager-yearly', 'Charts\StoreManagerYearlyChartController@response')->name('charts.store-manager-yearly.index');
    Route::get('charts/store-manager-monthly', 'Charts\StoreManagerMonthlyChartController@response')->name('charts.store-manager-monthly.index');
    Route::get('charts/regional-manager-yearly', 'Charts\RegionalManagerYearlyChartController@response')->name('charts.regional-manager-yearly.index');
    Route::get('charts/regional-manager-monthly', 'Charts\RegionalManagerMonthlyChartController@response')->name('charts.regional-manager-monthly.index');
    Route::get('charts/regional-manager-popular-product', 'Charts\RegionalManagerPopularProductChartController@response')->name('charts.regional-manager-popular-product.index');
    Route::get('charts/investor-yearly', 'Charts\InvestorYearlyChartController@response')->name('charts.investor-yearly.index');
    Route::get('charts/investor-monthly', 'Charts\InvestorMonthlyChartController@response')->name('charts.investor-monthly.index');
    Route::get('charts/popular-product', 'Charts\PopularProductChartController@response')->name('charts.popular-product.index');
    Route::get('charts/investor-popular-product', 'Charts\InvestorPopularProductChartController@response')->name('charts.investor-popular-product.index');
    Route::get('charts/direktur-yearly', 'Charts\DirekturYearlyChartController@response')->name('charts.direktur-yearly.index');
    Route::get('charts/direktur-monthly', 'Charts\DirekturMonthlyChartController@response')->name('charts.direktur-monthly.index');
    Route::get('charts/direktur-popular-product', 'Charts\DirekturPopularProductChartController@response')->name('charts.direktur-popular-product.index');
    Route::get('charts/admin-yearly', 'Charts\AdminYearlyChartController@response')->name('charts.admin-yearly.index');
    Route::get('charts/admin-monthly', 'Charts\AdminMonthlyChartController@response')->name('charts.admin-monthly.index');
    Route::get('charts/admin-popular-product', 'Charts\AdminPopularProductChartController@response')->name('charts.admin-popular-product.index');
    Route::crud('payment-method', 'PaymentMethodCrudController');
    Route::crud('voucher', 'VoucherCrudController');
    Route::post('Api/DetailTransaction', 'AdminController@details');
    Route::crud('good-inbound', 'GoodInboundCrudController');
}); // this should be the absolute last line of this file
<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $superadmin = Role::create([
            'name' => 'superadmin',
        ]);

        $admin = Role::create([
            'name' => 'admin',
        ]);

        $direktur = Role::create([
            'name' => 'direktur',
        ]);

        $investor = Role::create([
            'name' => 'investor',
        ]);

        $regional_manager = Role::create([
            'name' => 'regional manager',
        ]);

        $store_manager = Role::create([
            'name' => 'store manager',
        ]);

        $kasir = Role::create([
            'name' => 'kasir',
        ]);

        $operator = Role::create([
            'name' => 'operator',
        ]);
    }
}

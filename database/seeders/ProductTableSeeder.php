<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item1 = Product::create([
            'code' => 'CPN',
            'name' => 'Capucino',
            'price' => '14500.20',
            'product_type_id' => '2',
        ]);
        $item2 = Product::create([
            'code' => 'SPG',
            'name' => 'Spagheti',
            'price' => '21500.50',
            'product_type_id' => '1',
        ]);
        $playground = Product::create([
            'code' => 'PLG',
            'name' => 'Playground',
            'price' => '35000',
            'holiday_price' => '40000',
            'product_type_id' => '3',
        ]);
        $race = Product::create([
            'code' => 'RCE',
            'name' => 'Race',
            'price' => '20000',
            'holiday_price' => '20000',
            'product_type_id' => '4',
            'timer' => '15',
        ]);
    }
}

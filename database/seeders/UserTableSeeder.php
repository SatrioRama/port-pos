<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = User::create([
            'name' => 'Satrio Rama',
            'username' => 'satrio',
            'email' => 'ramadhanu@anomali.co.id',
            'password' => bcrypt('password')
        ]);

        $user1->assignRole('superadmin');

        $user2 = User::create([
            'name' => 'Irvanda Auldrien',
            'username' => 'irvanda',
            'email' => 'irvanda@anomali.co.id',
            'password' => bcrypt('password')
        ]);

        $user2->assignRole('superadmin');

        $user3 = User::create([
            'name' => 'Sayoga Setya Dewangga',
            'username' => 'Sayoga17',
            'email' => 'sayoga@aksa.co.id',
            'password' => bcrypt('password')
        ]);

        $user3->assignRole('admin');

        $user4 = User::create([
            'name' => 'Erwin Nugraha',
            'username' => 'erwin',
            'email' => 'erwin@aksa.co.id',
            'password' => bcrypt('password')
        ]);

        $user4->assignRole('direktur');

        $user5 = User::create([
            'name' => 'Yogyantoro',
            'username' => 'yogie',
            'email' => 'yogyantoro@aksa.co.id',
            'password' => bcrypt('password')
        ]);

        $user5->assignRole('direktur');

        $user6 = User::create([
            'name' => 'Fani Sintya Pratiwi',
            'username' => 'fani',
            'email' => 'fani@aksa.co.id',
            'password' => bcrypt('password')
        ]);

        $user6->assignRole('direktur');

    }
}

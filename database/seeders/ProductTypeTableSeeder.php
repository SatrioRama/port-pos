<?php

namespace Database\Seeders;

use App\Models\ProductType;
use Illuminate\Database\Seeder;

class ProductTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Food = ProductType::create([
            'name' => 'Food',
            'flag' => '0',
        ]);
        $Baverages = ProductType::create([
            'name' => 'Beverages',
            'flag' => '0',
        ]);
        $unlimited = ProductType::create([
            'name' => 'Unlimited',
            'flag' => '1',
        ]);
        $timer = ProductType::create([
            'name' => 'Timer',
            'flag' => '1',
        ]);
    }
}

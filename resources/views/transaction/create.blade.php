@extends(backpack_view('blank'))

@php
    $user = backpack_user();
    $shift = \App\Models\Shift::where('user_id', '=', backpack_user()->id)->where('store_branch_id', '=', backpack_user()->storeBranch->first()->id)->where('active', '=', App\Status::ACTIVE)->first();
    $store = \App\Models\StoreBranch::find($shift->store_branch_id);
    $productType = $store->type->productType;
@endphp

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! 'Create '.$crud->entity_name !!}</small>
	    </h2>
    </section>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        @include('transaction.product')
    </div>
    <div class="col-md-6">
        @include('transaction.list-cart')
    </div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
    @livewireStyles
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.8.2/dist/alpine.min.js" defer></script>
    @livewireScripts
@endsection

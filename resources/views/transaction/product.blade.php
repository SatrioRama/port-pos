@foreach ($productType as $type)
    <div class="card no-padding no-border">
        <div class="card-header">
            {{$type->name}}
        </div>
        <div class="card-body">
            @foreach ($type->product as $product)
                @if (!empty($product->storeBranch->whereIn('id', backpack_user()->storeBranch->pluck('id'))->first()))
                    @livewire('list-product', ['product' => $product], key($product->id))
                @endif
            @endforeach
        </div>
    </div>
@endforeach

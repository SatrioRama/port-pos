<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <style>
        @media print {
            #printPageButton {
                display: none;
            }
            #backButton {
                display: none;
            }
        }
    </style>
    <title>Ticket {{$data->transaction_number}}</title>
</head>
<body>
    <button id="backButton" onClick="window.history.back();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>KEMBALI</strong></button>
    <br>
    <br>
    <button id="printPageButton" onClick="window.print();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>PRINT</strong></button>
    @foreach ($data->details as $item)
        @if ($item->product->type->flag == App\Flag::ALC_RACE)
            @for ($i = 0; $i < $item->qty; $i++)

                <table cellspacing="0" border="0" style="width: 100%">
                    <tr>
                        <td style="font-size: 200%"><center>{{$data->storebranch->name}}</center></td>
                    </tr>
                    <tr>
                        <td><center>{{$data->storebranch->address}}</center></td>
                    </tr>
                    <tr>
                        <td><center>Ticket</center></td>
                    </tr>
                </table>
                <table cellspacing="0" border="0" style="width: 100%">
                    <tr>
                        <td style="width: 50%">Receipt Number</td>
                        <td style="width: 1%">:</td>
                        <td style="width: 49%">{{$data->transaction_number}}</td>
                    </tr>
                    <tr>
                        <td>Date</td>
                        <td>:</td>
                        <td>{{date('d-m-Y, H:i:s',strtotime($data->created_at))}}</td>
                    </tr>
                </table>
                <hr>
                <table cellspacing="0" border="0" style="width: 100%">
                    <tr>
                        <th>{{$item->product->name}}</th>
                    </tr>
                </table>
                <hr>
                <table cellspacing="0" border="0" style="width: 100%; text-align: right">
                    <tr>
                        <td><center>Hanya berlaku untuk 1x (Satu Kali)<br>bermain pada tanggal {{date('d-m-Y',strtotime($data->created_at))}}</center></td>
                    </tr>
                    <tr>
                        <td><center>Tukarkan tiket ini kepada petugas kami untuk bermain</center></td>
                    </tr>
                </table>
                <br>
                <table cellspacing="0" border="0" style="width: 100%; text-align: center">
                    <tr>
                        <td>* * Selamat Bermain * *</td>
                    </tr>
                </table>
                @if ($i+1 < $item->qty)
                    <div style="page-break-after: always;"></div>
                @endif
            @endfor
        @elseif ($item->product->type->flag == App\Flag::ARCADE)
            <table cellspacing="0" border="0" style="width: 100%">
                <tr>
                    <td style="font-size: 200%"><center>{{$data->storebranch->name}}</center></td>
                </tr>
                <tr>
                    <td><center>{{$data->storebranch->address}}</center></td>
                </tr>
                <tr>
                    <td><center>Ticket</center></td>
                </tr>
            </table>
            <table cellspacing="0" border="0" style="width: 100%">
                <tr>
                    <td style="width: 50%">Receipt Number</td>
                    <td style="width: 1%">:</td>
                    <td style="width: 49%">{{$data->transaction_number}}</td>
                </tr>
                <tr>
                    <td>Date</td>
                    <td>:</td>
                    <td>{{date('d-m-Y, H:i:s',strtotime($data->created_at))}}</td>
                </tr>
            </table>
            <hr>
            <table cellspacing="0" border="0" style="width: 100%">
                <tr>
                    <th>{{$item->product->name}}</th>
                </tr>
                <tr>
                    <th>{{$item->qty}}</th>
                </tr>
            </table>
            <hr>
            <table cellspacing="0" border="0" style="width: 100%; text-align: right">
                <tr>
                    <td><center>Hanya dapat ditukar 1x (Satu Kali)<br>pada tanggal {{date('d-m-Y',strtotime($data->created_at))}}</center></td>
                </tr>
                <tr>
                    <td><center>Tukarkan tiket ini kepada petugas kami untuk bermain</center></td>
                </tr>
            </table>
            <br>
            <table cellspacing="0" border="0" style="width: 100%; text-align: center">
                <tr>
                    <td>* * Selamat Bermain * *</td>
                </tr>
            </table>
        @endif
        @if ($loop->last)
        @else
            <div style="page-break-after: always;"></div>
        @endif
    @endforeach
</body>
</html>

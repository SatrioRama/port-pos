@extends(backpack_view('blank'))

@section('header')
	<section class="container-fluid d-print-none">
    	<a href="javascript: window.print();" class="btn float-right"><i class="la la-print"></i></a>
		<h2>
	        <span class="text-capitalize">{!! $crud->getHeading() ?? $crud->entity_name_plural !!}</span>
	        <small>{!! $crud->getSubheading() ?? mb_ucfirst(trans('backpack::crud.preview')).' '.$crud->entity_name !!}</small>
	        @if ($crud->hasAccess('list'))
	          <small class=""><a href="{{ route('transaction.create') }}" class="font-sm"><i class="la la-angle-double-left"></i> Back to <span>{{ $crud->entity_name_plural }}</span></a></small>
	        @endif
	    </h2>
    </section>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card col-md-12">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <img src="/logo.png" height="100%" class="card-img-top" alt="PT. Lintang Aksa Calandra">
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-6">
                            <h1>Transaksi Sukses</h1>
                        </div>
                        <div class="col-md-6">
                            <h4 class="float-right" style="font-weight:100">{{ date('d M Y, h:i',strtotime($crud->entry->created_at)) }}</h4>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <h4 class="card-text">Nomor Transaksi : <strong>{{$crud->entry->transaction_number}}</strong></h4>
                    @if (!empty(@$crud->entry->approval_code))
                        <br>
                        <h4 class="card-text">Approval Code : <strong>{{@$crud->entry->approval_code}}</strong></h4>
                    @endif
                </div>
                <ul class="list-group list-group-flush">
                    <div class="table">
                        <table class="table no-border">
                            <tr>
                                <td><h4>Subtotal</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->total, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Discount</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->discount, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Total</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->total_amount, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Pembayaran Tunai</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->cash, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                            <tr>
                                <td><h4>Kembali</h4></td>
                                <td><h4><strong>Rp{{number_format(@$crud->entry->change, 2, ',', '.')}}</strong></h4></td>
                            </tr>
                        </table>
                    </div>
                </ul>
                <div class="card-footer">
                    <div class="row p-2">
                        @if ($crud->entry->productType->whereIn('flag', [\App\Flag::ALC_RACE, \App\Flag::ARCADE])->count('id') > 0)
                            @if ($crud->entry->struk_print == false)
                                <div class="col-md-6">
                                    <a class="col-md-12 btn btn-secondary" href="{{ backpack_url('transaction/struk/'.$crud->entry->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK STRUK BELANJA</strong></a>
                                </div>
                            @endif
                            @if ($crud->entry->ticket_print == false)
                                <div class="col-md-6">
                                    <a class="col-md-12 btn btn-primary" href="{{ backpack_url('transaction/ticket/'.$crud->entry->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK TIKET WAHANA</strong></a>
                                </div>
                            @endif
                        @else
                            <div class="col-md-12">
                                <a class="col-md-12 btn btn-secondary" href="{{ backpack_url('transaction/struk/'.$crud->entry->id.'/print') }}"><i class="fa fa-print fa-fw"></i> <strong>CETAK STRUK BELANJA</strong></a>
                            </div>
                        @endif
                    </div>
                    @if (backpack_user()->hasRole('kasir') && !empty(\App\Models\Shift::where('user_id', '=', @backpack_user()->id)->where('store_branch_id', '=', @backpack_user()->storeBranch->first()->id)->where('active', '=', App\Status::ACTIVE)->first()))
                        <div class="row p-2">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <a href="{{ route('transaction.create') }}" class="col-md-12 btn btn-success"><strong>MULAI TRANSAKSI BARU</strong></a>
                            </div>
                            <div class="col-md-4"></div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after_styles')
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/crud.css') }}">
	<link rel="stylesheet" href="{{ asset('packages/backpack/crud/css/show.css') }}">
@endsection

@section('after_scripts')
	<script src="{{ asset('packages/backpack/crud/js/crud.js') }}"></script>
	<script src="{{ asset('packages/backpack/crud/js/show.js') }}"></script>
@endsection

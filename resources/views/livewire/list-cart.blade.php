
<div class="table">
    <table class="table no-border" id="listCart">
        <thead>
            <tr>
                <th width="20%">Item Name</th>
                <th width="20%">Qty</th>
                <th width="20%">Price</th>
                <th width="25%">Subtotal</th>
                <th width="15%">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($carts as $product)
                <tr>
                    <td>{{$product->name}}</td>
                    <td><input id="qty" type="number" wire.model="qty" wire:change="check($event.target.value, '{{$product->rowId}}')" name="qty" value="{{$product->qty}}" class="form-control" aria-label="Sizing example input" aria-describedby="qty"></td>
                    <td>Rp{{number_format($product->price, 2, ',', '.')}}</td>
                    <td>Rp{{number_format($product->subtotal, 2, ',', '.')}}</td>
                    <td>
                        <button type="button" style="width:100%" class="btn btn-success" wire:click="plusToCart('{{$product->rowId}}')">
                        &nbsp; <i class='nav-icon fa fa-plus'></i> &nbsp;
                        </button>
                        <button type="button" style="width:100%" class="btn btn-warning mt-1" wire:click="minusToCart('{{$product->rowId}}')">
                        &nbsp; <i class='nav-icon fa fa-minus'></i> &nbsp;
                        </button>
                        <button type="button" style="width:100%" class="btn btn-danger mt-1" wire:click="destroy('{{$product->rowId}}')">
                        &nbsp; <i class='nav-icon fa fa-trash'></i> &nbsp;
                        </button>
                    </td>
                </tr>
                <input style="border: none" type="hidden" name="product_id[]" value="{{$product->id}}">
                <input style="border: none" type="hidden" name="product_qty[]" value="{{$product->qty}}">
                <input style="border: none" type="hidden" name="product_price[]" value="{{$product->price}}">
                <input style="border: none" type="hidden" name="product_total[]" value="{{$product->subtotal}}">
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" class="text-right"><strong>Total</strong></td>
                <td colspan="2"><strong>Rp{{Cart::subtotal(2, ',', '.')}}</strong></td>
                <input id="total_amount" style="border: none" type="hidden" name="total_amount" value="{{Cart::subtotal(2, '.', '')}}">
            </tr>
        </tfoot>
    </table>
</div>

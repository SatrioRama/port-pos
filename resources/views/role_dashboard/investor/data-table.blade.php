<div class="row">
    <div class="col-md-12">
        <div class="card border-danger col-md-12">
            <div class="card-header text-center">Data Table Transaksi Bulan {{Carbon\Carbon::parse(request('show_dashboard_date', date('Y-m-d')))->format('M Y')}}</div>
            <div class="card-body">
                <table id="data-table" class="display nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th class="text-center">Nomor</th>
                            <th class="text-center">Tanggal</th>
                            @foreach (backpack_user()->storeBranch as $store)
                                <th class="text-center">{{$store->name}}</th>
                            @endforeach
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($investor['monthly'] as $key => $date)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-center">{{$date}}</td>
                            @foreach (backpack_user()->storeBranch as $store)
                                @php
                                    $modal_perhari = $store->operational/count($investor['monthly']);
                                    $check = App\Models\TransactionDetail::whereIn('product_id', backpack_user()->userProduct->pluck('id'))
                                    ->whereHas('transaction', function (Illuminate\Database\Eloquent\Builder $query) use ($store) {
                                        $query->where('store_branch_id', '=', $store->id);
                                    })
                                    ->where('created_at', '>=', backpack_user()->date_join)
                                    ->whereDate('created_at', date('Y-m-d', strtotime($date)));
                                    $data = App\Investor::calc($check->sum('sub_total'), $modal_perhari, $store->tax, backpack_user()->percentage);
                                @endphp
                                    <td class="text-center">Rp{{number_format($data, 2, ',', '.')}}</td>
                            @endforeach
                            @php
                                $modal_perhari = $store->operational/count($investor['monthly']);
                                $check = App\Models\TransactionDetail::whereIn('product_id', backpack_user()->userProduct->pluck('id'))
                                ->where('created_at', '>=', backpack_user()->date_join)
                                ->whereDate('created_at', date('Y-m-d', strtotime($date)));
                                $data = App\Investor::calc($check->sum('sub_total'), $modal_perhari, $store->tax, backpack_user()->percentage);
                            @endphp
                                <td class="text-center"><strong>Rp{{number_format($data, 2, ',', '.')}}</strong></td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th class="text-center" colspan="2">Total</th>
                            @foreach (backpack_user()->storeBranch as $store)
                                @php
                                    $check = App\Models\TransactionDetail::whereIn('product_id', backpack_user()->userProduct->pluck('id'))
                                        ->whereHas('transaction', function (Illuminate\Database\Eloquent\Builder $query) use ($store) {
                                            $query->where('store_branch_id', '=', $store->id);
                                        })
                                        ->where('created_at', '>=', backpack_user()->date_join)
                                        ->whereMonth('created_at', date('m', strtotime($investor['monthly'][0])))->whereYear('created_at', $investor['yearly']['year']);
                                    $data = App\Investor::calc($check->sum('sub_total'), $modal_perhari, $store->tax, backpack_user()->percentage);
                                @endphp
                                    <th class="text-center">Rp{{number_format($data, 2, ',', '.')}}</th>
                            @endforeach
                            @php
                                $check = App\Models\TransactionDetail::whereIn('product_id', backpack_user()->userProduct->pluck('id'))
                                    ->where('created_at', '>=', backpack_user()->date_join)
                                    ->whereMonth('created_at', date('m', strtotime($investor['monthly'][0])))->whereYear('created_at', $investor['yearly']['year']);
                                $data = App\Investor::calc($check->sum('sub_total'), $modal_perhari, $store->tax, backpack_user()->percentage);
                            @endphp
                                <th class="text-center">Rp{{number_format($data, 2, ',', '.')}}</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

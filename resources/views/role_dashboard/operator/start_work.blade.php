<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
              Selamat Bekerja
            </div>
            <div class="card-body">
              <h5 class="card-title">Operator {{backpack_user()->storeBranch->first()->name}}</h5>
              <p class="card-text">pastikan setiap pengguna memiliki tiket</p>
              <a href="{{ backpack_url('vehicle') }}" class="btn btn-primary">Mulai Bekerja</a>
            </div>
        </div>
    </div>
</div>

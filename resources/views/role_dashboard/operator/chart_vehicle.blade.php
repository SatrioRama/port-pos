<div class="row">
    <div class="col-md-12">
        @php
        Widget::add([
            'type'    => 'div',
            'class'   => 'row',
            'content' => [ // widgets
                ['type'       => 'chart',
                'controller' => \App\Http\Controllers\Admin\Charts\VehicleUsedChartController::class,
                'wrapper' => ['class' => 'col text-center'],
                'content' => ['header' => 'Penggunaan Wahana'],
                ]
            ]
        ])->section('after_content');
        @endphp
    </div>
</div>

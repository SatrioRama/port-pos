@extends(backpack_view('blank'))

@section('content')
@include('role_dashboard.kasir.cashier')
@include('role_dashboard.kasir.chart_kasir')
@endsection
@include('role_dashboard.kasir.startshift')

@section('after_styles')
@endsection

@section('after_scripts')
@endsection

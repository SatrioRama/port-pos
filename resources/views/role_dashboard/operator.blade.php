@extends(backpack_view('blank'))

@section('content')
@include('role_dashboard.operator.start_work')
@include('role_dashboard.operator.chart_vehicle')
@endsection

@section('after_styles')
@endsection

@section('after_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
@endsection

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css">
        <style>
            @media print {
                #printPageButton {
                    display: none;
                }
                #backButton {
                    display: none;
                }
            }
        </style>
        <title>Shift {{$data->id}} - {{$data->user->name}}</title>
    </head>
    <body>

    <button id="backButton" onClick="window.history.back();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>KEMBALI</strong></button>
    <br>
    <br>
    <button id="printPageButton" onClick="window.print();" style="width: 100%"><i class="fa fa-print fa-fw"></i> <strong>PRINT</strong></button>

    <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="font-size: 200%"><center>{{$data->storebranch->name}}</center></td>
            </tr>
            <tr>
                <td><center>{{$data->storebranch->address}}</center></td>
            </tr>
            <tr>
                <td><center>{{$data->storebranch->city}}</center></td>
            </tr>
        </table>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 50%">Shift ID</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%">{{$data->id}}</td>
            </tr>
            <tr>
                <td>Cashier</td>
                <td>:</td>
                <td>{{$data->user->name}}</td>
            </tr>
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="text-align: left; width: 25%">{{date('d-m-Y',strtotime($data->created_at))}}</td>
                <td style="text-align: center; width: 50%">Rekap Shift</td>
                <td style="text-align: right; width: 25%">{{date('H:i:s',strtotime($data->created_at))}}</td>
            </tr>
        </table>
        <hr>
        <h2>Transaksi</h2>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 50%">Jumlah Transaksi</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%">{{count($data->transaction)}}</td>
            </tr>
            <tr>
                <td>Jumlah Uang Transaksi</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->total_transaction, 2, ',', '.')}}</td>
            </tr>
        </table>
        <hr>
        <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 50%">Mulai Shift</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%">{{date('d M Y, H:i:s',strtotime(@$data->open_at))}}</td>
            </tr>
            <tr>
                <td>Selesai Shift</td>
                <td>:</td>
                <td>{{date('d M Y, H:i:s',strtotime(@$data->close_at))}}</td>
            </tr>
        </table>
                    <hr>
                    @php
                        $details = App\Models\TransactionDetail::whereIn('transaction_id', $data->transaction->pluck('id')->toArray())->get();
                        $item_ids = App\Models\TransactionDetail::select('product_id')->whereIn('transaction_id', $data->transaction->pluck('id')->toArray())->distinct('product_id')->pluck('product_id')->toArray();
                        $products = App\Models\Product::whereIn('id' ,$item_ids)->get();
                    @endphp
                    <table cellspacing="0" border="0" style="width: 100%">
                        @foreach ($products as $product)
                            <tr>
                                <td style="width: 50%; text-align: left">{{$product->name}}</td>
                                <td style="width: 1%">:</td>
                                <td style="width: 49%; text-align: left">{{@$details->where('product_id', '=', $product->id)->sum('qty')}}</td>
                            </tr>
                        @endforeach
                        <tr>
                            <td style="text-align: left">Total</td>
                            <td style="width: 1%">:</td>
                            <td style="text-align: left">{{@$details->sum('qty')}}</td>
                        </tr>
                    </table>
                    <hr>
            <table cellspacing="0" border="0" style="width: 100%">
            <tr>
                <td style="width: 50%">Uang Tunai Saat Mulai</td>
                <td style="width: 1%">:</td>
                <td style="width: 49%">Rp{{number_format(@$data->begining_cash, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total Pemasukan Cash</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->transaction_cash, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total Pemasukan EDC</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->transaction_edc, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total Refund</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->refund, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total Pemasukan</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->total_transaction, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Total Setoran Uang</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->expected_cash, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Jumlah Uang Kasir</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->actual_cash, 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Selisih</td>
                <td>:</td>
                <td>Rp{{number_format(@$data->difference, 2, ',', '.')}}</td>
            </tr>
        </table>
        <br>
        {{-- <table cellspacing="0" border="0" style="width: 100%; text-align: center">
            <tr>
                <td><i class="las la-globe-americas"></i> www.alcrace.com</td>
            </tr>
            <tr>
                <td><i class="lab la-instagram"></i> &commat;alcraceofficial</td>
            </tr>
        </table> --}}
    </body>
</html>


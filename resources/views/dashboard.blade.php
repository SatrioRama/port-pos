@extends(backpack_view('blank'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            @php
            Widget::add([
                'type'        => 'jumbotron',
                'heading'     => 'ERROR = "Account"',
                'content'     => 'Mohon segera menghubungi pimpinan anda untuk konfigurasi akun anda.',
                'button_link' => backpack_url('logout'),
                'button_text' => 'Logout',
            ])->section('before_content');
            @endphp
        </div>
    </div>
@endsection

@section('after_styles')
@endsection

@section('after_scripts')
@endsection

<?php
namespace App;

class Status {

    const INACTIVE  = 0;
    const ACTIVE    = 1;

}

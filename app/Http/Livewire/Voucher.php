<?php

namespace App\Http\Livewire;

use App\Models\Voucher as ModelsVoucher;
use Gloudemans\Shoppingcart\Facades\Cart;
use Livewire\Component;

class Voucher extends Component
{
    public $voucher_code;

    public function render()
    {
        $voucher_code = $this->voucher_code;
        return view('livewire.voucher', compact(['voucher_code']));
    }

    public function check($value) {
        $dates = date(today());
        $find_voucher = ModelsVoucher::where('active', '=', true)->where('voucher_code', '=', $value)->whereDate('start_date', '<=', $dates)->whereDate('end_date', '>=', $dates)->first();
        if (!empty($find_voucher)) {
            $product_contain = $find_voucher->product->pluck('id');
            $find = Cart::content()->whereIn('id', $product_contain)->first();
            if (!empty($find_voucher) && !empty($find)) {
                $check_get_discount = count(Cart::content()->where('weight', '!=', 0));
                if ($check_get_discount == 0) {
                    if ($find->subtotal - $find_voucher->disc >= 0) {
                        Cart::update($find->rowId, ['weight' => $find_voucher->disc]);
                    } else {
                        Cart::update($find->rowId, ['weight' => $find->subtotal]);
                    }
                }
            }
        } else {
            $check = Cart::content()->where('weight', '!=', 0)->first();
            if (!empty($check)) {
                Cart::update($check->rowId, ['weight' => 0]);
            }
        }
        $this->voucher_code = $value;
        $this->emit('cart_updated');
    }
}

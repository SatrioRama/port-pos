<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartMoney extends Component
{
    protected $listeners = ['cart_updated' => 'render'];

    public function render()
    {
        $total = (int)Cart::subtotal(2, ',', '');
        $total -= Cart::content()->sum('weight');
        if ($total < 0) {
            $total = 0;
        }
        return view('livewire.cart-money', compact('total'));
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Vehicle;
use App\Models\VehicleTime;
use Carbon\Carbon;
use Livewire\Component;

class ListVehicle extends Component
{
    public $vehicle;

    protected $listeners = ['time_updated' => 'render', 'refreshComponent' => '$refresh'];

    public function render()
    {
        $vehicle = $this->vehicle;
        $times = VehicleTime::where('vehicle_id', '=', $vehicle->id)->where('status', '=', true)->first();
        if (!empty($times) && $times->end_at > date(Carbon::now())) {
            $t1 = Carbon::parse($times->end_at);
            $t2 = Carbon::now();
            $diff = $t2->diff($t1);
            $diff_times = $diff->format('%i:%s');
            $status = $times->status;
        }elseif (!empty($times) && $times->end_at < date(Carbon::now())) {
            $diff_times = "00:00";
            $status = $times->status;
        }else{
            $diff_times = "00:00";
            $status = false;
        }
        return view('livewire.list-vehicle', compact(['vehicle', 'diff_times', 'times']));
    }

    public function addTime($id)
    {
        $times = Vehicle::find($id);
        $arr = explode(":", $times->been_used, 2);
        $day = floor((int)$arr[0]/24);
        if ($day > 0) {
            $time_been_used = Carbon::today()->addDay($day)->format('d-m-Y')." ".(int)$arr[0]%24 .":".$arr[1];
        }else {
            $time_been_used = $times->been_used;
        }
        $t1 = Carbon::parse($time_been_used)->addMinute($times->product->timer);
        $t2 = Carbon::today();
        $time = $t2->diff($t1)->format($t2->diffInHours($t1).':%I:%S');
        $times->been_used = $time;
        $times->update();
        $save = new VehicleTime();
        $save->vehicle_id = $times->id;
        $save->start_at = Carbon::now();
        $save->end_at = Carbon::now()->addMinutes($times->product->timer);
        $save->user_id = backpack_user()->id;
        $save->status = true;
        $save->save();

        $this->emit('time_updated');

        \Alert::add('success', 'Success add time')->flash();
    }

    public function changeStatus($id)
    {
        $update = VehicleTime::findOrFail($id);
        $update->status = false;
        $update->update();

        $this->emit('time_updated');

        \Alert::add('success', 'Success Change Status')->flash();
    }
}

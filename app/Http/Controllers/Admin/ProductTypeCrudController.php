<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductTypeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Flag;

/**
 * Class ProductTypeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductTypeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ProductType::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/producttype');
        CRUD::setEntityNameStrings('Product Types', 'Product Type');
        $this->crud->setShowView('config.producttype.show');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        if (backpack_user()->hasRole('store manager')) {
            $this->crud->removeButtons(['create', 'update', 'delete']);
        }
        $this->crud->addColumn([
            'name'      => 'row_number',
            'type'      => 'row_number',
            'label'     => 'Nomor',
            'orderable' => false,
        ])->makeFirstColumn();
        $this->crud->addColumn('name');
        $this->crud->addColumn([
            // select_from_array
            'name'    => 'flag',
            'label'   => 'Category',
            'type'    => 'select_from_array',
            'options' => [Flag::NORMAL => 'Normal', Flag::ALC_RACE => 'Wahana', Flag::ARCADE => 'Arcade'],
        ]);
        $this->crud->addColumn([
            // relationship count
            'name'      => 'product', // name of relationship method in the model
            'type'      => 'relationship_count',
            'label'     => 'Store', // Table column heading
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    if ($entry->flag == Flag::ALC_RACE || $entry->flag == Flag::ARCADE) {
                        return backpack_url('ticket/?ticket_type_id=%5B"'.$entry->id.'"%5D');
                    } elseif ($entry->flag == Flag::NORMAL) {
                        return backpack_url('product/?product_type_id=%5B"'.$entry->id.'"%5D');
                    } else {
                        return "-";
                    }

                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
            // OPTIONAL
            // 'suffix' => ' tags', // to show "123 tags" instead of "123 items"
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductTypeRequest::class);

        $this->crud->addField([
            // select_from_array
            'name'    => 'flag',
            'label'   => 'Category',
            'type'    => 'select_from_array',
            'options' => [Flag::NORMAL => 'Normal', Flag::ALC_RACE => 'Wahana', Flag::ARCADE => 'Arcade'],
        ]);
        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}

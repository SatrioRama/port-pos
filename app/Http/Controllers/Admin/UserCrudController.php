<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserRequest;
use App\Models\Product;
use App\Models\Role;
use App\Models\StoreBranch;
use App\Models\User;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController as BackpackUserCrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserCrudController extends BackpackUserCrudController
{
    protected function addUserFields()
    {
        CRUD::setValidation(UserRequest::class);
        $this->crud->addFields([
            [
                'tab'   => 'Profile',
                'name'  => 'name',
                'label' => trans('backpack::permissionmanager.name'),
                'type'  => 'text',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'tab'   => 'Profile',
                'name'  => 'email',
                'label' => trans('backpack::permissionmanager.email'),
                'type'  => 'email',
                'attributes' => [
                    'required' => false,
                ]
            ],
            [
                'tab'   => 'Profile',
                'name'  => 'username',
                'label' => 'Username',
                'type'  => 'text',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'tab'   => 'Profile',
                'name'  => 'password',
                'label' => trans('backpack::permissionmanager.password'),
                'type'  => 'password',
                'attributes' => [
                    'required' => true,
                ]
            ],
            [
                'tab'   => 'Profile',
                'name'  => 'password_confirmation',
                'label' => trans('backpack::permissionmanager.password_confirmation'),
                'type'  => 'password',
                'attributes' => [
                    'required' => true,
                ]
            ],[    // Select2Multiple = n-n relationship (with pivot table)
                'tab'       => 'Profile',
                'label'     => "Store",
                'type'      => 'select2_multiple',
                'name'      => 'storeBranch', // the method that defines the relationship in your Model

                // optional
                'entity'    => 'storeBranch', // the method that defines the relationship in your Model
                'model'     => "App\Models\StoreBranch", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                'select_all' => true, // show Select All and Clear buttons?

                'options'   => (function ($query) {
                    if (backpack_user()->hasAnyRole(['store manager', 'regional manager'])) {
                        return StoreBranch::whereIn('id', backpack_user()->storeBranch->pluck('id'))->get();
                    } else {
                        return StoreBranch::get();
                    }
                })
            ],
        ]);
        if (backpack_user()->hasRole('store manager')) {
            $this->crud->addField([   // Checklist
                'tab'       => 'Profile',
                'label'     => 'Roles',
                'type'      => 'checklist',
                'name'      => 'roles',
                'entity'    => 'roles',
                'attribute' => 'name',
                'model'     => config('permission.models.role'),
                'pivot'     => true,
                'options'   => (function ($query) {
                    return $query->whereIn('name', ['operator', 'kasir'])->pluck('name', 'id');
                }),
            ]);
        } elseif (backpack_user()->hasRole('regional manager')) {
            $this->crud->addField([   // Checklist
                'tab'       => 'Profile',
                'label'     => 'Roles',
                'type'      => 'checklist',
                'name'      => 'roles',
                'entity'    => 'roles',
                'attribute' => 'name',
                'model'     => config('permission.models.role'),
                'pivot'     => true,
                'options'   => (function ($query) {
                    return $query->whereIn('name', ['operator', 'kasir', 'store manager'])->pluck('name', 'id');
                }),
            ]);
        } elseif (backpack_user()->hasRole('admin')) {
            $url = url()->current();
            $prefix = url('admin/user/');
            $replace1 = str_replace($prefix."/","",$url);
            $replace2 = str_replace("/edit","",$replace1);
            if (stripos($url, '/edit') != false && count(User::find($replace2)->storeBranch) > 0 && User::find($replace2)->hasRole('investor')) {
                $this->crud->addField([   // date_picker
                    'tab'       => 'Profile',
                    'name'  => 'date_join',
                    'type'  => 'date_picker',
                    'label' => 'Date',

                    // optional:
                    'date_picker_options' => [
                       'todayBtn' => 'linked',
                    ],
                 ]);
                $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
                    'tab'       => 'Profile',
                    'label'     => "Product",
                    'type'      => 'select2_multiple',
                    'name'      => 'userProduct', // the method that defines the relationship in your Model

                    // optional
                    'entity'    => 'userProduct', // the method that defines the relationship in your Model
                    'model'     => "App\Models\Product", // foreign key model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                    'select_all' => true, // show Select All and Clear buttons?

                    'options'   => (function ($query) {
                        return $query->whereHas('storeBranch', function (Builder $query) {
                            $url = url()->current();
                            $prefix = url('admin/user/');
                            $replace1 = str_replace($prefix."/","",$url);
                            $replace2 = str_replace("/edit","",$replace1);
                            $query->whereIn('store_branch_id', User::find($replace2)->storeBranch->pluck('id'));
                        })->get();
                    })
                ],);
                $this->crud->addField([   // Number
                    'tab'       => 'Profile',
                    'name' => 'percentage',
                    'label' => 'Bagi Hasil',
                    'type' => 'number',

                    // optionals
                    // 'attributes' => ["step" => "any"], // allow decimals
                    // 'prefix'     => "$",
                    'suffix'     => "%",
                ]);
            }
            $this->crud->addField([   // Checklist
                'tab'       => 'Profile',
                'label'     => 'Roles',
                'type'      => 'checklist',
                'name'      => 'roles',
                'entity'    => 'roles',
                'attribute' => 'name',
                'model'     => config('permission.models.role'),
                'pivot'     => true,
                'options'   => (function ($query) {
                    return $query->whereNotIn('name', ['superadmin'])->pluck('name', 'id');
                }),
            ]);
        } elseif (backpack_user()->hasRole('superadmin')) {
            $url = url()->current();
            $prefix = url('admin/user/');
            $replace1 = str_replace($prefix."/","",$url);
            $replace2 = str_replace("/edit","",$replace1);
            if (stripos($url, '/edit') != false && count(User::find($replace2)->storeBranch) > 0 && User::find($replace2)->hasRole('investor')) {
                $this->crud->addField([   // date_picker
                    'tab'       => 'Profile',
                    'name'  => 'date_join',
                    'type'  => 'date_picker',
                    'label' => 'Date',
                    'hint' => 'Date Join',

                    // optional:
                    'date_picker_options' => [
                       'todayBtn' => 'linked',
                    ],
                 ]);
                $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
                    'tab'       => 'Profile',
                    'label'     => "Product",
                    'type'      => 'select2_multiple',
                    'name'      => 'userProduct', // the method that defines the relationship in your Model

                    // optional
                    'entity'    => 'userProduct', // the method that defines the relationship in your Model
                    'model'     => "App\Models\Product", // foreign key model
                    'attribute' => 'name', // foreign key attribute that is shown to user
                    'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                    'select_all' => true, // show Select All and Clear buttons?

                    'options'   => (function ($query) {
                        return Product::whereHas('storeBranch', function (Builder $query) {
                            $url = url()->current();
                            $prefix = url('admin/user/');
                            $replace1 = str_replace($prefix."/","",$url);
                            $replace2 = str_replace("/edit","",$replace1);
                            $query->whereIn('store_branch_id', User::find($replace2)->storeBranch->pluck('id'));
                        })->get();
                    })
                ],);
                $this->crud->addField([   // Number
                    'tab'       => 'Profile',
                    'name' => 'percentage',
                    'label' => 'Bagi Hasil',
                    'type' => 'number',

                    // optionals
                    // 'attributes' => ["step" => "any"], // allow decimals
                    // 'prefix'     => "$",
                    'suffix'     => "%",
                ]);
            }
            $this->crud->addField([
                'tab'               => 'Profile',
                // two interconnected entities
                'label'             => 'Roles',
                'field_unique_name' => 'user_role_permission',
                'type'              => 'checklist_dependency',
                'name'              => ['roles', 'permissions'],
                'subfields'         => [
                    'primary' => [
                        'label'            => '',
                        'name'             => 'roles', // the method that defines the relationship in your Model
                        'entity'           => 'roles', // the method that defines the relationship in your Model
                        'entity_secondary' => 'permissions', // the method that defines the relationship in your Model
                        'attribute'        => 'name', // foreign key attribute that is shown to user
                        'model'            => config('permission.models.role'), // foreign key model
                        'pivot'            => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns'   => 3, //can be 1,2,3,4,6
                    ],
                    'secondary' => [
                        'label'          => '',
                        'name'           => 'permissions', // the method that defines the relationship in your Model
                        'entity'         => 'permissions', // the method that defines the relationship in your Model
                        'entity_primary' => 'roles', // the method that defines the relationship in your Model
                        'attribute'      => 'name', // foreign key attribute that is shown to user
                        'model'          => config('permission.models.permission'), // foreign key model
                        'pivot'          => true, // on create&update, do you need to add/delete pivot table entries?]
                        'number_columns' => 3, //can be 1,2,3,4,6
                    ],
                ],
            ]);
        }
    }
}

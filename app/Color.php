<?php
namespace App;

class Color {

    static function hex2rgb($hex)
    {
        list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
        return [$r,$g,$b];
    }
}
